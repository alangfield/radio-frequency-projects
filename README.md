# Radio Frequency Projects

## Thesis
The extent of the radio frequenecy work has involved but is not limited to observation of the radio frequency emission from deep-space objects.
The objects in particular we observed were in the 21cm or 1420MHz line, otherwise known as the hydrogen spectrum. 

The project being funded by COSGC (Colorado Space Grant Consotrium) was reviewed and held at a competition with over 50 other COSGC  runner-ups
and went on to win first place.

## Abstract 
On a daily basis from the surface of earth, we're being bombarded by an enormous amount of light spectrum from space. Everything from 
microwaves, to gamma-rays. The light that we're most interested in focusing on would be the radio frequency emission from hydrogen objects.
Objects such as gas clouds, and nebula, and galaxies. The telescope I've built is a 12 foot in diameter parabolic dish that will take in
radio emission and use specific hardware and software elements to process the data. 

The hardware most importanly used in my setup includes but is not limited to:

Low noise amplifiers 
Down converters
Dickie switches
Software defined radios 
Feedhorn 
DC power supplies
Actuators 

I tried many different variations of the hardware components to try to optimize the data and record the best observations

## Data

After reviewing hundreds of millions of data points, and hundreds of data sets we begin to understand the common trends of hydrogen objects.
Each data point will vary but with repition we were able to transpose data and have a decent understand of the hydrogen concentrations of deep-space
objects. 

Collecting data can be a long enduring task. The data I've collected will require a type of scan known as a drift scan. Drift scans are formally
known as such; The telescope is pointed at a certain coordinate location in the sky 3 to 4 hours before the arrival of an object. Once the object
passes into the FOV (field of view) of the central feed horn location, we'll see a large spike in hydrogen emission being observed from the star.
The software will collect the data until the object is passed and must remain untouched for another 3 to 4 hours to level out the data. Offering 
a "bell curve" of data.

## Initial Results 
Our initial reults were difficult to interpret and required dozens of times of recalibration, and sorting out subtle issues with the telescope in
order to get a basis on reasonable data. After rooughly 20 scans we were able to sort out the complications in the telescopes hardware.

## Furute Work
The telescope was designed for everyone to be able to use, with a slight learning curve and dedication, the average person can obtain the knowledge 
to use the radio telescope. There is a ton of work that can still be done on the telescope, and below is a list I've worked on and continue to work
with students to move through new challenge with.

Recalibrate feedhord
Trench dug for command center
Actuators mounted
Coordinate system hardware/software implemented
Wires from control room burried

## Readings


